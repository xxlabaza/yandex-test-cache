package ru.xxlabaza.test.cache;

import java.util.Map;
import org.junit.Assert;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 12:44:27 AM
 * <p>
 * @version 1.0.0
 */
public final class AssertHelper {

    public static <K, V> void assertMap (Map<K, V> expectedMap, Map<K, V> map) {
        Assert.assertNotNull(map);
        Assert.assertEquals(expectedMap.size(), map.size());
        expectedMap.entrySet().stream().forEach((entry) -> {
            V value = map.get(entry.getKey());
            Assert.assertNotNull(value);
            Assert.assertEquals(entry.getValue(), value);
        });
    }
}
