package ru.xxlabaza.test.cache.medium;

import java.util.HashMap;
import org.junit.Test;
import ru.xxlabaza.test.cache.AssertHelper;

/**
 *
 * <p>
 * @author Artem Labazin (xxlabaza@gmail.com)
 * <p>
 * @since Mar 29, 2015 | 12:42:47 AM
 * <p>
 * @version 1.0.0
 */
public class LruCacheTest {

    @Test
    public void testPut () {
        Cache<Integer, String> cache = new LruCache<>(3);

        cache.put(1, "one");
        cache.put(2, "two");
        cache.put(3, "three");
        AssertHelper.assertMap(new HashMap<Integer, String>() {

            {
                put(1, "one");
                put(2, "two");
                put(3, "three");
            }
        }, cache);

        cache.put(4, "four");
        AssertHelper.assertMap(new HashMap<Integer, String>() {

            {
                put(2, "two");
                put(3, "three");
                put(4, "four");
            }
        }, cache);

        cache.get(2);
        cache.put(5, "five");
        AssertHelper.assertMap(new HashMap<Integer, String>() {

            {
                put(2, "two");
                put(4, "four");
                put(5, "five");
            }
        }, cache);
    }

}
