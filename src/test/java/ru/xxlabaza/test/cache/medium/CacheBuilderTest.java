package ru.xxlabaza.test.cache.medium;

import java.util.Map;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 12:54:13 AM
 * <p>
 * @version 1.0.0
 */
public class CacheBuilderTest {

    @Rule
    public ExpectedException EXPECTED_EXCEPTION = ExpectedException.none();

    @Test
    public void testSetCacheType_CacheTypeIsNotSet () {
        CacheBuilder cacheBuilder = CacheBuilder.create();
        Assert.assertNotNull(cacheBuilder);

        cacheBuilder.setCacheStrategyType(CacheStrategyType.LRU);
        cacheBuilder.setMaxSize(3);

        EXPECTED_EXCEPTION.expect(IllegalArgumentException.class);
        EXPECTED_EXCEPTION.expectMessage("Cache type must be set");
        cacheBuilder.build();
    }

    @Test
    public void testSetCacheStrategy_CacheStrategyIsNotSet () {
        CacheBuilder cacheBuilder = CacheBuilder.create();
        Assert.assertNotNull(cacheBuilder);

        cacheBuilder.setCacheType(CacheType.UNSYNCHRONIZED);
        cacheBuilder.setMaxSize(3);

        EXPECTED_EXCEPTION.expect(IllegalArgumentException.class);
        EXPECTED_EXCEPTION.expectMessage("Cache strategy type must be set");
        cacheBuilder.build();
    }

    @Test
    public void testSetCacheStrategy_CacheMaxSizeIsNotSet () {
        CacheBuilder cacheBuilder = CacheBuilder.create();
        Assert.assertNotNull(cacheBuilder);

        cacheBuilder.setCacheType(CacheType.UNSYNCHRONIZED);
        cacheBuilder.setCacheStrategyType(CacheStrategyType.LRU);

        EXPECTED_EXCEPTION.expect(IllegalArgumentException.class);
        EXPECTED_EXCEPTION.expectMessage("Cache max size must be greater than 0");
        cacheBuilder.build();
    }

    @Test
    public void testBuild_LruCache () {
        CacheBuilder cacheBuilder = CacheBuilder.create();
        Assert.assertNotNull(cacheBuilder);

        cacheBuilder.setCacheType(CacheType.UNSYNCHRONIZED);
        Assert.assertEquals(CacheType.UNSYNCHRONIZED, cacheBuilder.getCacheType());

        cacheBuilder.setCacheStrategyType(CacheStrategyType.LRU);
        Assert.assertEquals(CacheStrategyType.LRU, cacheBuilder.getCacheStrategyType());

        cacheBuilder.setMaxSize(3);
        Assert.assertEquals(3, cacheBuilder.getMaxSize());

        Map<Integer, String> cache = cacheBuilder.build();
        Assert.assertNotNull(cache);
        Assert.assertTrue(cache instanceof Cache);
        Assert.assertTrue(cache instanceof LruCache);
        Cache<Integer, String> cacheTyped = (Cache<Integer, String>) cache;
        Assert.assertEquals(3, cacheTyped.getMaxSize());
    }

    @Test
    public void testBuild_FifoCache () {
        CacheBuilder cacheBuilder = CacheBuilder.create();
        Assert.assertNotNull(cacheBuilder);

        cacheBuilder.setCacheType(CacheType.UNSYNCHRONIZED);
        Assert.assertEquals(CacheType.UNSYNCHRONIZED, cacheBuilder.getCacheType());

        cacheBuilder.setCacheStrategyType(CacheStrategyType.FIFO);
        Assert.assertEquals(CacheStrategyType.FIFO, cacheBuilder.getCacheStrategyType());

        cacheBuilder.setMaxSize(3);
        Assert.assertEquals(3, cacheBuilder.getMaxSize());

        Map<Integer, String> cache = cacheBuilder.build();
        Assert.assertNotNull(cache);
        Assert.assertTrue(cache instanceof Cache);
        Assert.assertTrue(cache instanceof FifoCache);
        Cache<Integer, String> cacheTyped = (Cache<Integer, String>) cache;
        Assert.assertEquals(3, cacheTyped.getMaxSize());
    }

    @Test
    public void testBuild_MruCache () {
        CacheBuilder cacheBuilder = CacheBuilder.create();
        Assert.assertNotNull(cacheBuilder);

        cacheBuilder.setCacheType(CacheType.UNSYNCHRONIZED);
        Assert.assertEquals(CacheType.UNSYNCHRONIZED, cacheBuilder.getCacheType());

        cacheBuilder.setCacheStrategyType(CacheStrategyType.MRU);
        Assert.assertEquals(CacheStrategyType.MRU, cacheBuilder.getCacheStrategyType());

        cacheBuilder.setMaxSize(3);
        Assert.assertEquals(3, cacheBuilder.getMaxSize());

        Map<Integer, String> cache = cacheBuilder.build();
        Assert.assertNotNull(cache);
        Assert.assertTrue(cache instanceof Cache);
        Assert.assertTrue(cache instanceof MruCache);
        Cache<Integer, String> cacheTyped = (Cache<Integer, String>) cache;
        Assert.assertEquals(3, cacheTyped.getMaxSize());
    }

    @Test
    public void testBuild_Synchronized () {
        CacheBuilder cacheBuilder = CacheBuilder.create();
        Assert.assertNotNull(cacheBuilder);

        cacheBuilder.setCacheType(CacheType.SYNCHRONIZED);
        Assert.assertEquals(CacheType.SYNCHRONIZED, cacheBuilder.getCacheType());

        cacheBuilder.setCacheStrategyType(CacheStrategyType.MRU);
        Assert.assertEquals(CacheStrategyType.MRU, cacheBuilder.getCacheStrategyType());

        cacheBuilder.setMaxSize(3);
        Assert.assertEquals(3, cacheBuilder.getMaxSize());

        Map<Integer, String> cache = cacheBuilder.build();
        Assert.assertNotNull(cache);
    }
}
