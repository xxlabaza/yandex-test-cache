package ru.xxlabaza.test.cache.hard;

import java.util.HashMap;
import org.junit.Test;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 30, 2015 | 3:28:07 AM
 * <p>
 * @version 1.0.0
 */
public class MruCacheTest {

    @Test
    public void testPut () {
        Cache<Integer, String> cache = new MruCache<>(3);

        cache.put(1, "one");
        cache.put(2, "two");
        cache.put(3, "three");
        AssertHelper.assertCache(new HashMap<Integer, String>() {

            {
                put(1, "one");
                put(2, "two");
                put(3, "three");
            }
        }, cache);

        cache.put(4, "four");
        AssertHelper.assertCache(new HashMap<Integer, String>() {

            {
                put(1, "one");
                put(2, "two");
                put(4, "four");
            }
        }, cache);

        cache.get(1);
        cache.put(5, "five");
        AssertHelper.assertCache(new HashMap<Integer, String>() {

            {
                put(2, "two");
                put(4, "four");
                put(5, "five");
            }
        }, cache);
    }
}
