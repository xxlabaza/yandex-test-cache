package ru.xxlabaza.test.cache.hard;

import java.util.HashMap;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 4:38:42 AM
 * <p>
 * @version 1.0.0
 */
public class FixedSizeCacheTest {

    @Rule
    public ExpectedException EXPECTED_EXCEPTION = ExpectedException.none();

    @Test
    public void testGetSize () {
        Cache<Integer, String> cache = new FixedSizeCache<>(3);

        cache.put(1, "one");
        Assert.assertEquals(1, cache.getSize());

        cache.put(1, "one");
        Assert.assertEquals(1, cache.getSize());

        cache.put(2, "two");
        Assert.assertEquals(2, cache.getSize());

        cache.put(2, "two");
        Assert.assertEquals(2, cache.getSize());
    }

    @Test
    public void testContains () {
        Cache<Integer, String> cache = new FixedSizeCache<>(3);

        cache.put(1, "one");
        Assert.assertTrue(cache.contains(1));

        cache.put(2, "two");
        Assert.assertTrue(cache.contains(2));
    }

    @Test
    public void testRemove () {
        Cache<Integer, String> cache = new FixedSizeCache<>(3);

        cache.put(1, "one");
        Assert.assertTrue(cache.contains(1));
        Assert.assertEquals(1, cache.getSize());

        cache.remove(1);
        Assert.assertFalse(cache.contains(1));
        Assert.assertEquals(0, cache.getSize());
    }

    @Test
    public void testPutAndGet () {
        Cache<Integer, String> cache = new FixedSizeCache<>(3);

        cache.put(1, "one");
        AssertHelper.assertCache(new HashMap<Integer, String>() {

            {

                put(1, "one");
            }
        }, cache);

        cache.put(1, "one");
        AssertHelper.assertCache(new HashMap<Integer, String>() {

            {

                put(1, "one");
            }
        }, cache);

        cache.put(null, "null");
        AssertHelper.assertCache(new HashMap<Integer, String>() {

            {

                put(null, "null");
                put(1, "one");
            }
        }, cache);

        cache.put(2, "two");
        AssertHelper.assertCache(new HashMap<Integer, String>() {

            {

                put(null, "null");
                put(1, "one");
                put(2, "two");
            }
        }, cache);
    }

    @Test
    public void testPutAndGet_CacheIsFull () {
        Cache<Integer, String> cache = new FixedSizeCache<>(3);
        cache.put(1, "one");
        cache.put(2, "two");
        cache.put(3, "three");

        EXPECTED_EXCEPTION.expect(IllegalArgumentException.class);
        EXPECTED_EXCEPTION.expectMessage("Cache is full");
        cache.put(4, "four");
    }
}
