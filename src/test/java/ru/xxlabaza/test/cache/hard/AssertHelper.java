package ru.xxlabaza.test.cache.hard;

import java.util.Map;
import org.junit.Assert;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 30, 2015 | 3:30:20 AM
 * <p>
 * @version 1.0.0
 */
public final class AssertHelper {

    public static <K, V> void assertCache (Map<K, V> expectedValues, Cache<K, V> cache) {
        Assert.assertNotNull(cache);
        Assert.assertEquals(expectedValues.size(), cache.getSize());
        expectedValues.entrySet().stream().forEach((entry) -> {
            V value = cache.get(entry.getKey());
            Assert.assertNotNull("Key \'" + entry.getKey() + "\' has no value", value);
            Assert.assertEquals(entry.getValue(), value);
        });
    }

    private AssertHelper () {
    }
}
