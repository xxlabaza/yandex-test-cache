package ru.xxlabaza.test.cache.hard;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 30, 2015 | 3:15:29 AM
 * <p>
 * @version 1.0.0
 */
public enum CacheStrategyType {

    LRU,
    MRU,
    FIFO;
}
