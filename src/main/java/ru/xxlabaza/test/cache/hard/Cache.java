package ru.xxlabaza.test.cache.hard;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 1:09:33 AM
 * <p>
 * @version 1.0.0
 */
public interface Cache<K, V> {

    void put (K key, V value);

    V get (K key);

    V remove (K key);

    boolean contains (K key);

    int getSize ();

    int getMaxSize ();

    boolean isFull ();
}
