package ru.xxlabaza.test.cache.hard;

import java.util.Objects;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 1:34:25 AM
 * <p>
 * @version 1.0.0
 */
class FixedSizeCache<K, V> implements Cache<K, V> {

    private final int maxSize;

    private final CacheElement<K, V>[] elements;

    private int size;

    FixedSizeCache (int maxSize) {
        this.maxSize = maxSize;
        elements = new CacheElement[maxSize + 1];
    }

    @Override
    public void put (K key, V value) {
        int hash = createHash(key);
        int index = createIndex(hash);

        CacheElement<K, V> element = findCacheElement(hash, key, index);
        if (element != null) {
            element.setValue(value);
            return;
        }

        if (isFull()) {
            throw new IllegalArgumentException("Cache is full");
        }

        CacheElement<K, V> firstInLinkedList = elements[index];
        elements[index] = new CacheElement<>(hash, key, value, firstInLinkedList);
        size++;
    }

    @Override
    public V get (K key) {
        int hash = createHash(key);
        int index = createIndex(hash);

        CacheElement<K, V> element = findCacheElement(hash, key, index);
        return (element == null)
               ? null
               : element.getValue();
    }

    @Override
    public V remove (K key) {
        int hash = createHash(key);
        int index = createIndex(hash);

        CacheElement<K, V> previous = null;
        CacheElement<K, V> current = elements[index];
        while (current != null) {
            if (current.getHash() == hash && ((key == current.getKey()) || current.getKey().equals(key))) {
                if (previous == null) {
                    elements[index] = null;
                } else {
                    previous.setNext(current.getNext());
                }
                size--;
                return current.getValue();
            }

            previous = current;
            current = current.getNext();
        }

        return null;
    }

    @Override
    public boolean contains (K key) {
        int hash = createHash(key);
        int index = createIndex(hash);

        return findCacheElement(hash, key, index) != null;
    }

    @Override
    public int getSize () {
        return size;
    }

    @Override
    public int getMaxSize () {
        return maxSize;
    }

    @Override
    public boolean isFull () {
        return size >= maxSize;
    }

    private int createHash (K key) {
        if (key == null) {
            return 0;
        }
        int hash = key.hashCode();
        hash ^= (hash >>> 20) ^ (hash >>> 12);
        return hash ^ (hash >>> 7) ^ (hash >>> 4);
    }

    private int createIndex (int hash) {
        return hash & (elements.length - 1);
    }

    private CacheElement<K, V> findCacheElement (int hash, K key, int index) {
        for (CacheElement<K, V> element = elements[index]; element != null; element = element.getNext()) {
            if (element.getHash() == hash && ((key == element.getKey()) || element.getKey().equals(key))) {
                return element;
            }
        }
        return null;
    }

    private class CacheElement<K, V> {

        private final int hash;

        private final K key;

        private V value;

        private CacheElement<K, V> next;

        CacheElement (int hash, K key, V value, CacheElement<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public int getHash () {
            return hash;
        }

        public K getKey () {
            return key;
        }

        public void setValue (V value) {
            this.value = value;
        }

        public V getValue () {
            return value;
        }

        public void setNext (CacheElement<K, V> next) {
            this.next = next;
        }

        public CacheElement<K, V> getNext () {
            return next;
        }

        @Override
        public int hashCode () {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        @Override
        public boolean equals (Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof CacheElement) {
                CacheElement<K, V> e = (CacheElement<K, V>) o;
                if (Objects.equals(key, e.getKey()) &&
                    Objects.equals(value, e.getValue())) {
                    return true;
                }
            }
            return false;
        }
    }
}
