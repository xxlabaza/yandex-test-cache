package ru.xxlabaza.test.cache.hard;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 30, 2015 | 1:48:50 AM
 * <p>
 * @version 1.0.0
 */
abstract class AbstractLinkedElementValueCache<K, V> implements Cache<K, V> {

    private final Cache<K, LinkedElementValue<K, V>> cache;

    private LinkedElementValue<K, V> head;

    private LinkedElementValue<K, V> tail;

    AbstractLinkedElementValueCache (int maxSize) {
        cache = new FixedSizeCache<>(maxSize);
    }

    @Override
    public boolean contains (K key) {
        return cache.contains(key);
    }

    @Override
    public int getSize () {
        return cache.getSize();
    }

    @Override
    public int getMaxSize () {
        return cache.getMaxSize();
    }

    @Override
    public boolean isFull () {
        return cache.isFull();
    }

    protected void replaceElementValue (K key, V value) {
        LinkedElementValue<K, V> linkedElementValue = cache.get(key);
        linkedElementValue.setValue(value);
    }

    protected void removeHeadElement () {
        if (head == null) {
            return;
        }
        cache.remove(head.getKey());
        head = head.getNext();
        if (head == null) {
            tail = null;
        } else {
            head.setPrevious(null);
        }
    }

    protected void removeTailElement () {
        if (tail == null) {
            return;
        }
        cache.remove(tail.getKey());
        tail = tail.getPrevious();
        if (tail == null) {
            head = null;
        } else {
            tail.setNext(null);
        }
    }

    protected V popUpElement (K key) {
        LinkedElementValue<K, V> linkedElementValue = cache.get(key);
        removeElementLinks(linkedElementValue);
        linkedElementValue.setNext(head);
        if (head != null) {
            head.setPrevious(linkedElementValue);
        }
        head = linkedElementValue;
        if (tail == null) {
            tail = linkedElementValue;
        }
        return linkedElementValue.getValue();
    }

    protected void addElementToHead (K key, V value) {
        LinkedElementValue<K, V> newLinkedElementValue = new LinkedElementValue<>(key, value);
        if (head == null) {
            tail = newLinkedElementValue;
        } else {
            head.setPrevious(newLinkedElementValue);
            newLinkedElementValue.setNext(head);
        }
        head = newLinkedElementValue;
        cache.put(key, newLinkedElementValue);
    }

    protected void addElementToTail (K key, V value) {
        LinkedElementValue<K, V> newLinkedElementValue = new LinkedElementValue<>(key, value);
        if (tail == null) {
            head = newLinkedElementValue;
        } else {
            tail.setNext(newLinkedElementValue);
            newLinkedElementValue.setPrevious(tail);
        }
        tail = newLinkedElementValue;
        cache.put(key, newLinkedElementValue);
    }

    protected V removeElement (K key) {
        LinkedElementValue<K, V> linkedElementValue = cache.remove(key);
        if (linkedElementValue == null) {
            return null;
        }
        removeElementLinks(linkedElementValue);
        return linkedElementValue.getValue();
    }

    protected V getElementValue (K key) {
        LinkedElementValue<K, V> linkedElementValue = cache.get(key);
        return (linkedElementValue == null)
               ? null
               : linkedElementValue.getValue();
    }

    private void removeElementLinks (LinkedElementValue<K, V> linkedElement) {
        LinkedElementValue<K, V> next = linkedElement.getNext();
        LinkedElementValue<K, V> previous = linkedElement.getPrevious();

        if (previous != null) {
            previous.setNext(next);
        } else {
            head = next;
        }

        if (next != null) {
            next.setPrevious(previous);
        } else {
            tail = previous;
        }
    }

    private class LinkedElementValue<K, V> {

        private final K key;

        private V value;

        private LinkedElementValue<K, V> next;

        private LinkedElementValue<K, V> previous;

        LinkedElementValue (K key, V value) {
            this(key, value, null, null);
        }

        LinkedElementValue (K key, V value, LinkedElementValue<K, V> next, LinkedElementValue<K, V> previous) {
            this.key = key;
            this.value = value;
            this.next = next;
            this.previous = previous;
        }

        public K getKey () {
            return key;
        }

        public void setValue (V value) {
            this.value = value;
        }

        public V getValue () {
            return value;
        }

        public void setNext (LinkedElementValue<K, V> next) {
            this.next = next;
        }

        public LinkedElementValue<K, V> getNext () {
            return next;
        }

        public void setPrevious (LinkedElementValue<K, V> previous) {
            this.previous = previous;
        }

        public LinkedElementValue<K, V> getPrevious () {
            return previous;
        }
    }
}
