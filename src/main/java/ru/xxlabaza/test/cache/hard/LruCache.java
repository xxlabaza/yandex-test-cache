package ru.xxlabaza.test.cache.hard;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 5:01:46 AM
 * <p>
 * @version 1.0.0
 */
class LruCache<K, V> extends AbstractLinkedElementValueCache<K, V> {

    LruCache (int maxSize) {
        super(maxSize);
    }

    @Override
    public void put (K key, V value) {
        if (contains(key)) {
            replaceElementValue(key, value);
            popUpElement(key);
            return;
        }

        if (isFull()) {
            removeTailElement();
        }
        addElementToHead(key, value);
    }

    @Override
    public V get (K key) {
        if (!contains(key)) {
            return null;
        }
        return popUpElement(key);
    }

    @Override
    public V remove (K key) {
        return removeElement(key);
    }
}
