package ru.xxlabaza.test.cache.hard;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 5:03:18 AM
 * <p>
 * @version 1.0.0
 */
class FifoCache<K, V> extends AbstractLinkedElementValueCache<K, V> {

    FifoCache (int maxSize) {
        super(maxSize);
    }

    @Override
    public void put (K key, V value) {
        if (contains(key)) {
            replaceElementValue(key, value);
            return;
        }

        if (isFull()) {
            removeHeadElement();
        }
        addElementToTail(key, value);
    }

    @Override
    public V get (K key) {
        return getElementValue(key);
    }

    @Override
    public V remove (K key) {
        return removeElement(key);
    }
}
