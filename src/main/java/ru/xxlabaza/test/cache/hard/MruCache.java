package ru.xxlabaza.test.cache.hard;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 30, 2015 | 3:10:33 AM
 * <p>
 * @version 1.0.0
 */
class MruCache<K, V> extends LruCache<K, V> {

    MruCache (int maxSize) {
        super(maxSize);
    }

    @Override
    public void put (K key, V value) {
        if (contains(key)) {
            replaceElementValue(key, value);
            popUpElement(key);
            return;
        }

        if (isFull()) {
            removeHeadElement();
        }
        addElementToHead(key, value);
    }
}
