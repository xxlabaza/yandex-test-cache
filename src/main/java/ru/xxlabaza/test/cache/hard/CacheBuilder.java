package ru.xxlabaza.test.cache.hard;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 30, 2015 | 3:14:46 AM
 * <p>
 * @version 1.0.0
 */
public class CacheBuilder {

    public static CacheBuilder create () {
        return new CacheBuilder();
    }

    private CacheType cacheType;

    private CacheStrategyType cacheStrategyType;

    private int maxSize;

    private CacheBuilder () {
    }

    public CacheType getCacheType () {
        return cacheType;
    }

    public CacheBuilder setCacheType (CacheType cacheType) {
        this.cacheType = cacheType;
        return this;
    }

    public CacheStrategyType getCacheStrategyType () {
        return cacheStrategyType;
    }

    public CacheBuilder setCacheStrategyType (CacheStrategyType cacheStrategyType) {
        this.cacheStrategyType = cacheStrategyType;
        return this;
    }

    public int getMaxSize () {
        return maxSize;
    }

    public CacheBuilder setMaxSize (int maxSize) {
        this.maxSize = maxSize;
        return this;
    }

    public <K, V> Cache<K, V> build () {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("Cache max size must be greater than 0");
        } else if (cacheStrategyType == null) {
            throw new IllegalArgumentException("Cache strategy type must be set");
        } else if (cacheType == null) {
            throw new IllegalArgumentException("Cache type must be set");
        }

        Cache<K, V> cache;
        switch (cacheStrategyType) {
        case FIFO:
            cache = new FifoCache<>(maxSize);
            break;
        case MRU:
            cache = new MruCache<>(maxSize);
            break;
        case LRU:
            cache = new LruCache<>(maxSize);
            break;
        default:
            throw new UnsupportedOperationException("");
        }

        switch (cacheType) {
        case SYNCHRONIZED:
            return new SynchronizedCache<>(cache);
        case UNSYNCHRONIZED:
            return cache;
        default:
            throw new UnsupportedOperationException("");
        }
    }

    private class SynchronizedCache<K, V> implements Cache<K, V> {

        private final Object mutex;

        private final Cache<K, V> cache;

        SynchronizedCache (Cache<K, V> cache) {
            this.cache = cache;
            mutex = this;
        }

        @Override
        public void put (K key, V value) {
            synchronized (mutex) {
                cache.put(key, value);
            }
        }

        @Override
        public V get (K key) {
            synchronized (mutex) {
                return cache.get(key);
            }
        }

        @Override
        public V remove (K key) {
            synchronized (mutex) {
                return cache.remove(key);
            }
        }

        @Override
        public boolean contains (K key) {
            synchronized (mutex) {
                return cache.contains(key);
            }
        }

        @Override
        public int getSize () {
            synchronized (mutex) {
                return cache.getSize();
            }
        }

        @Override
        public int getMaxSize () {
            synchronized (mutex) {
                return cache.getMaxSize();
            }
        }

        @Override
        public boolean isFull () {
            synchronized (mutex) {
                return cache.isFull();
            }
        }
    }
}
