package ru.xxlabaza.test.cache.easy;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 12:12:54 AM
 * <p>
 * @version 1.0.0
 */
public class FifoCache<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = 2326368924657070098L;

    private final int maxSize;

    public FifoCache (int maxSize) {
        super(maxSize + 1, 1.0F, false);
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry (Entry<K, V> eldest) {
        return size() > maxSize;
    }
}
