package ru.xxlabaza.test.cache.easy;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 29, 2015 | 12:06:36 AM
 * <p>
 * @version 1.0.0
 */
public class LruCache<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = 3634954378867224743L;

    private final int maxSize;

    public LruCache (int maxSize) {
        super(maxSize + 1, 1.0F, true);
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry (Entry<K, V> eldest) {
        return size() > maxSize;
    }
}
