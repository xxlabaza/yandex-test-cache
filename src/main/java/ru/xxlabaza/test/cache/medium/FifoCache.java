package ru.xxlabaza.test.cache.medium;

import java.util.Map.Entry;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 28, 2015 | 10:48:37 PM
 * <p>
 * @version 1.0.0
 */
class FifoCache<K, V> extends Cache<K, V> {

    private static final long serialVersionUID = 2326368924657070098L;

    FifoCache (int maxSize) {
        super(maxSize);
    }

    @Override
    protected boolean removeEldestEntry (Entry<K, V> eldest) {
        return isFull();
    }
}
