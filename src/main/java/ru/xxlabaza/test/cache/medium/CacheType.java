package ru.xxlabaza.test.cache.medium;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 28, 2015 | 10:16:19 PM
 * <p>
 * @version 1.0.0
 */
public enum CacheType {

    SYNCHRONIZED,
    UNSYNCHRONIZED;

}
