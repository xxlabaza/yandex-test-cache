package ru.xxlabaza.test.cache.medium;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 28, 2015 | 11:08:12 PM
 * <p>
 * @version 1.0.0
 */
class MruCache<K, V> extends Cache<K, V> {

    private static final long serialVersionUID = 7881537975904497675L;

    MruCache (int maxSize) {
        super(maxSize, true);
    }

    @Override
    public V put (K key, V value) {
        V previousValue = super.put(key, value);
        if (isFull()) {
            Object lastKey = keySet().toArray()[size() - 2];
            remove(lastKey);
        }
        return previousValue;
    }
}
