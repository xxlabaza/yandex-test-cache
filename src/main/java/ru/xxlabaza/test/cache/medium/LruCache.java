package ru.xxlabaza.test.cache.medium;

import java.util.Map.Entry;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 28, 2015 | 10:02:53 PM
 * <p>
 * @version 1.0.0
 */
class LruCache<K, V> extends Cache<K, V> {

    private static final long serialVersionUID = 3634954378867224743L;

    LruCache (int maxSize) {
        super(maxSize, true);
    }

    @Override
    protected boolean removeEldestEntry (Entry<K, V> eldest) {
        return isFull();
    }
}
