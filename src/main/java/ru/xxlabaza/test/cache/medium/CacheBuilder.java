package ru.xxlabaza.test.cache.medium;

import java.util.Collections;
import java.util.Map;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 28, 2015 | 10:10:49 PM
 * <p>
 * @version 1.0.0
 */
public final class CacheBuilder {

    public static CacheBuilder create () {
        return new CacheBuilder();
    }

    private CacheType cacheType;

    private CacheStrategyType cacheStrategyType;

    private int maxSize;

    private CacheBuilder () {
    }

    public CacheType getCacheType () {
        return cacheType;
    }

    public CacheBuilder setCacheType (CacheType cacheType) {
        this.cacheType = cacheType;
        return this;
    }

    public CacheStrategyType getCacheStrategyType () {
        return cacheStrategyType;
    }

    public CacheBuilder setCacheStrategyType (CacheStrategyType cacheStrategyType) {
        this.cacheStrategyType = cacheStrategyType;
        return this;
    }

    public int getMaxSize () {
        return maxSize;
    }

    public CacheBuilder setMaxSize (int maxSize) {
        this.maxSize = maxSize;
        return this;
    }

    public <K, V> Map<K, V> build () {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("Cache max size must be greater than 0");
        } else if (cacheStrategyType == null) {
            throw new IllegalArgumentException("Cache strategy type must be set");
        } else if (cacheType == null) {
            throw new IllegalArgumentException("Cache type must be set");
        }

        Map<K, V> cache;
        switch (cacheStrategyType) {
        case FIFO:
            cache = new FifoCache<>(maxSize);
            break;
        case MRU:
            cache = new MruCache<>(maxSize);
            break;
        case LRU:
            cache = new LruCache<>(maxSize);
            break;
        default:
            throw new IllegalArgumentException("Unknown cache strategy type - " + cacheStrategyType.name());
        }

        switch (cacheType) {
        case SYNCHRONIZED:
            return Collections.synchronizedMap(cache);
        case UNSYNCHRONIZED:
            return cache;
        default:
            throw new IllegalArgumentException("Unknown cache type - " + cacheType.name());
        }
    }
}
