package ru.xxlabaza.test.cache.medium;

import java.util.LinkedHashMap;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Mar 28, 2015 | 10:31:40 PM
 * <p>
 * @version 1.0.0
 */
abstract class Cache<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = 8724756214875532811L;

    private final int maxSize;

    Cache (int maxSize) {
        this(maxSize, false);
    }

    Cache (int maxSize, boolean accessOrder) {
        super(maxSize + 1, 1.0F, accessOrder);
        this.maxSize = maxSize;
    }

    public int getMaxSize () {
        return maxSize;
    }

    protected boolean isFull () {
        return size() > maxSize;
    }
}
